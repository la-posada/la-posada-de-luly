<?php
if (isset($_POST['submit'])) {
    if (!empty($_POST['nombre']) && !empty($_POST['email']) && !empty($_POST['fecha']) && !empty($_POST['productos']) && !empty($_POST['mensaje'])) {
        $nombre = $_POST['nombre'];
        $email = $_POST['email'];
        $telefono = $_POST['telefono'];
        $fecha = $_POST['fecha'];
        $productos = $_POST['productos'];

        if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
            $statusMsg = 'Hay un error en tu mail. Por favor, chequeá si lo escribiste correctamente.';
        }else{
            $uploadStatus = 1;

        if(!isset($productos)) {
            echo("<p>Por favor, seleccioná por lo menos un producto de tu preferencia.</p>\n");
        }else{
            $nProductos = count($productos);
            echo("<p>Seleccionaste $nProductos productos: ");
            for($i=0; $i < $nProductos; $i++) {
                echo($productos[$i] . " ");
            }echo("</p>");
        }

        $subidaArchivos = $_POST['subidaArchivos'];

        if(!empty($_FILES["subidaArchivos"]["name"])){
                
            // File path config
            $targetDir = "uploads/";
            $fileName = basename($_FILES["subidaArchivos"]["name"]);
            $targetFilePath = $targetDir . $fileName;
            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
            
            // Allow certain file formats
            $allowTypes = array('png', 'jpeg', 'jpg', 'gif', 'bmp');
            if(in_array($fileType, $allowTypes)){
                // Upload file to the server
                if(move_uploaded_file($_FILES["subidaArchivos"]["tmp_name"], $targetFilePath)){
                    $uploadedFile = $targetFilePath;
                }else{
                    $uploadStatus = 0;
                    $statusMsg = "Perdón, hubo un error en la subida de tus archivos.";
                }
            }else{
                $uploadStatus = 0;
                $statusMsg = 'Perdón, sólo se permite subir archivos con las extensiones: PNG, JPEG, JPG, GIF y BMP.';
            }
        }

        $mensaje = $_POST['mensaje'];
        $validarNewsletter = $_POST['validarNewsletter'];

        if(isset($validarNewsletter)) {
            echo("<p>Te estaremos enviando nuestras novedades.</p>")
        }

        $header = 'From: ' . $email . " \r\n ";
        $header.= "X-Mailer: PHP/" . phpversion() " \r\n ";

        $texto = "Este mensaje fue enviado por " . $nombre . " \r\n ";
        $texto = "Su email es " . $mail . " \r\n ";
        $texto = "Mensaje: " . $mensaje .  " \r\n ";
        $texto = "Fecha de entrega: " . $fecha . " \r\n ";
        $texto = "Productos elegidos: " . $productos . " \r\n "; 
        $texto = "Fotos adjuntas" . $subidaArchivos . " \r\n ";
        $texto = "Desea recibir novedades :" . $validarNewsletter . " \r\n ";


        $para = 'l.a.senderovsky@gmail.com';
        $asunto = 'Mensaje de mi sitio web'

        $mail = mail($para, $asunto, utf8_decode($texto), $header);
        if($mail){
            $statusMsg = 'Gracias por escribirme. Pronto te estaré respondiendo.';
            $msgClass = 'succdiv';
            
            $postData = '';
        }else{
            $statusMsg = 'El pedido de contacto falló. Por favor, envialo nuevamente.';
        }
    }else{
        $statusMsg = 'Por favor, completá todos los campos.';
    }
}


